export interface Task {
//properties
    id: string;
    title: string;
    description: string; //semi colons
    status: TaskStatus; //only allow one of the values in the enum 
}

export enum TaskStatus {
    OPEN = 'OPEN', //comas
    IN_PROGRESS = 'IN_PROGRESS',
    DONE = 'DONE'
}