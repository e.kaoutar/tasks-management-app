import { IsEnum, IsOptional, IsString } from "class-validator";
import { TaskStatus } from "../task.model";
import { TasksService } from "../tasks.service";

export class GetTasksFilterDto{
    @IsOptional()
    @IsEnum(TasksService)
    status?: TaskStatus;

    @IsOptional()
    @IsString()
    search?: string; // ? means it's optional
}