import { IsNotEmpty } from "class-validator";

export class CreateTaskDto {
    //properties of a task
    @IsNotEmpty()
    title: string;

    @IsNotEmpty()
    description: string;
}

//telling nest js to use the validation we added -> app level