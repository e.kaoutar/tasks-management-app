import { Body, Controller, Delete, Get, Param, Patch, Post, Query } from '@nestjs/common';
import { title } from 'process';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';
import { UpdateStatusDto } from './dto/update-task-status.dto';
import { Task, TaskStatus } from './task.model';
import { TasksService } from './tasks.service';

@Controller('tasks')
export class TasksController {
    //constructor with task service as param
    constructor(private tasksService: TasksService){} //inject task service in our controller
    //in typescript we don't need to mention this.tasksService ... the 'private' before the param definition does it.

    //whenever get request comes it
    @Get()
    getTasks(@Query() filterDto: GetTasksFilterDto) : Task[] {
        //if we have any filters defined, call tasksServiceWithFilter , otherwise call just task
        if(Object.keys(filterDto).length){
            //... Tasks based on some filters --> in service
            return this.tasksService.getTasksWithFilters(filterDto);
        } else {
            return this.tasksService.getAllTasks();
        }

    }

    //fetch task by id
    @Get('/:id') // : means path parameter
    getTaskById(@Param('id') id: string) : Task {
        return this.tasksService.getTaskById(id);
    }

    //delete task
    @Delete('/:id')
    deleteTask(@Param('id') id: string) : void {
        return this.tasksService.deleteTask(id);
    }

    //updtate task status
    @Patch('/:id/status')
    updateTaskStatus(@Param('id') id: string, @Body() updateStatusDto: UpdateStatusDto) : Task{ //reusing the enum status we created in the model
        //destructure is
        const { status } = updateStatusDto;
        return this.tasksService.updateTaskStatus(id, status);
    }

    //creation of a task = POST
    @Post()
    createTask(@Body() createTaskDto : CreateTaskDto) : Task { //form of the http request should be URL ENCODED
        //retrieve the task parameters from the request url
        // 1 . retrieve entire request body -> NEST takes the whole body and assign it to 'body' parameter
        /////console.log('title', title);
        /////console.log('description', description);

        // 2. call back the method in service and give it the parameters title & description
        return this.tasksService.createTask(createTaskDto); 

    }
}
