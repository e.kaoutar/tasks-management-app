import { Injectable, NotFoundException } from '@nestjs/common';
import { Task, TaskStatus } from './task.model';
import { v4 as uuid} from 'uuid';
import { CreateTaskDto } from './dto/create-task.dto';
import { GetTasksFilterDto } from './dto/get-tasks-filter.dto';

@Injectable()
export class TasksService {
    //static data for now
    private tasks: Task[] = []; //type of Task will be task array

    //set of methods that states how to use the service -- minimise error
    getAllTasks() : Task[]{ //by default in typescript a method is public by default (no need to mention it)
        return this.tasks;
    }
    // -- we know that the type of tasks we'll be getting is in type Task (interface) ==> same in controller

    //fetch task by Id
    getTaskById(id: string) : Task {
        //try to get task
        const found = this.tasks.find((task) => task.id = id); //if we have a task that has the id matching 

        //if not found, throw an error
        //otherwise, return the found task

        if(!found){
            throw new NotFoundException(`Task ${id} is not found`);
        } else{
            return found;
        }


        //return this.tasks.find((task) => task.id = id); //if we have a task that has the id matching -> return item
    }

    //filtering & searching
    getTasksWithFilters(filterDto: GetTasksFilterDto): Task[] {
        const { status, search } = filterDto;
        
        //define a temporary array to hold the result
        let tasks = this.getAllTasks();
        
        // do smtg with status
        if(status){
            tasks = tasks.filter((task) => task.status === status);
        }
        // do smtg with search
        if(search){
            tasks = tasks.filter((task) => {
                if(task.title.includes(search) || task.description.includes(search)){
                    return true;
                }
                return false;
            });
        }

        //return final result
        return tasks;
    }

    createTask(createTaskDto: CreateTaskDto) : Task { //only title and description are to be provided bcs id is generated & status is open by default
        //structuring syntax
        const { title, description } = createTaskDto; //because those are the only inputs we want 

        const task: Task = {
            id: uuid(),
            title,
            description,
            status: TaskStatus.OPEN

        };

        ///1. we need to push this task to the array ==> append/create/add
        this.tasks.push(task);

        //2. return this to the controller
        return task;
    }

    //deletion method
    deleteTask(id:string) : void { //no point to return smtg deleted!
        //filter or slice or else ... to delete
        const found = this.getTaskById(id); //just for the purpose of learning - later ORM will be used
        this.tasks = this.tasks.filter((task) => task.id !== found.id); //return the records that are not identical to the one we want to delete
    }

    //updating status
    updateTaskStatus(id: string, status: TaskStatus){
        //getting the task
        const task = this.getTaskById(id);
        task.status = status; //affecting the new status
        return task;
    }

}
