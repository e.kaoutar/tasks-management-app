import { ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';

async function bootstrap() {
  //creates a new app 
  const app = await NestFactory.create(AppModule); //root module == AppModule
  app.useGlobalPipes(new ValidationPipe());
  await app.listen(3000);
}
bootstrap();
