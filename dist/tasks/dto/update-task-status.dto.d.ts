import { TaskStatus } from "../task.model";
export declare class UpdateStatusDto {
    status: TaskStatus;
}
